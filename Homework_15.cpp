// Homework_15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "EvenNumbers.h"

const int N = 24;

int main()
{
    
    std::cout << "We have N = " << N << "\n"
        << "Now function gonna show every even number from 0 to " << N << "\n";

    for (int a = 0; a < N; ++a)
    {
        if (a % 2 == 0)
        {
            std::cout << a << "\n";
        }
        else
        {

        }
    
    };
    
    std::cout << "Next function gonna show every even number via separate function \n";

    // Type(bool, number); true - even; false - odd
    std::cout << "What number type you need? Even = 0; Odd = 1. Please enter \n";
    int W;
    std::cin >> W;
    std::cout << "Enter the number \n";
    int P;
    std::cin >> P;
    int Result = Type(W, P);
    std::cout << Result;
    
    

    /*int b = 0;

    for (int N = 30; b < N; ++b)
    {
        if (b % 2 != 0)
        {
            std::cout << b << "\n";
        }
        else
        {
        
        }

    }*/
    
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
